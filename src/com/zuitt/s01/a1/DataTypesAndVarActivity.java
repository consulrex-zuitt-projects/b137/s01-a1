package com.zuitt.s01.a1;

public class DataTypesAndVarActivity {
    public static void main(String[] args) {
        String firstName = "John";
        String lastName = "Doe";
        double engGrade = 85.67;
        double mathGrade = 86.75;
        double sciGrade = 80.35;
        double ave = (engGrade + mathGrade + sciGrade) / 3;
        System.out.println("name: " + firstName + " "+ lastName);
        System.out.println("average: " + ave);
    }
}
